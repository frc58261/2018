/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package org.usfirst.frc.team5826.robot;

import java.io.IOException;

import org.usfirst.frc.team5826.robot.commands.CurveDriveCommand;
import org.usfirst.frc.team5826.robot.commands.DriveCommand;
import org.usfirst.frc.team5826.robot.commands.GrabberTest;
import org.usfirst.frc.team5826.robot.commands.LogitechJoystickDrive;
import org.usfirst.frc.team5826.robot.commands.RyansXboxDrive;
import org.usfirst.frc.team5826.robot.commands.TeleopDriveCommand;
import org.usfirst.frc.team5826.robot.commands.TestCommand;
import org.usfirst.frc.team5826.robot.commands.TestWheelsCommand;
import org.usfirst.frc.team5826.robot.commands.TurnCommand;
import org.usfirst.frc.team5826.robot.commands.UpstairsSquareCommand;
import org.usfirst.frc.team5826.robot.commands.WaveServoCommand;
import org.usfirst.frc.team5826.robot.commands.XboxControllerArcadeDrive;
import org.usfirst.frc.team5826.robot.commands.auto.CenterBailOut;
import org.usfirst.frc.team5826.robot.commands.auto.CenterLeftSwitch;
import org.usfirst.frc.team5826.robot.commands.auto.CenterRightSwitch;
import org.usfirst.frc.team5826.robot.commands.auto.LeftStartLeftScale;
import org.usfirst.frc.team5826.robot.commands.auto.LeftStartLeftSwitch;
import org.usfirst.frc.team5826.robot.commands.auto.LeftStartRightScale;
import org.usfirst.frc.team5826.robot.commands.auto.LeftStartRightSwitch;
import org.usfirst.frc.team5826.robot.commands.auto.LiftTest;
import org.usfirst.frc.team5826.robot.commands.auto.RightStartLeftScale;
import org.usfirst.frc.team5826.robot.commands.auto.RightStartLeftSwitch;
import org.usfirst.frc.team5826.robot.commands.auto.RightStartRightScale;
import org.usfirst.frc.team5826.robot.commands.auto.RightStartRightSwitch;
import org.usfirst.frc.team5826.robot.commands.auto.SideBailOut;
import org.usfirst.frc.team5826.robot.commands.auto.StraightSwitch;
import org.usfirst.frc.team5826.robot.commands.master.RightSwitch;
import org.usfirst.frc.team5826.robot.subsystems.ClimberSubsystem;
import org.usfirst.frc.team5826.robot.subsystems.DrivebaseSubsystem;
import org.usfirst.frc.team5826.robot.subsystems.GrabberSubsystem;
import org.usfirst.frc.team5826.robot.subsystems.LiftSubsystem;


import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.CameraServer;
/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the TimedRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the build.properties file in the
 * project.
 */
public class Robot extends TimedRobot {	
	// Declare all of your subsystems as public static final
	public static final DrivebaseSubsystem SS_DRIVEBASE = new DrivebaseSubsystem();
	public static final LiftSubsystem SS_LIFT = new LiftSubsystem();
	public static final GrabberSubsystem SS_GRABBER = new GrabberSubsystem();
	public static final ClimberSubsystem SS_CLIMB = new ClimberSubsystem();
	public static final OI oi = new OI();
	Compressor c = new Compressor(6);
	Command currentCommand;
	SendableChooser<Command> autoChooser;
	SendableChooser<Command> teleopChooser;
	
	SendableChooser<StartingPosition> startingPositionChooser;
	SendableChooser<Destination> destinationChooser;
	SendableChooser<AutoMode> autoModeChooser;
	/**
	 * This function is run when the robot is first started up and should be used
	 * for any initialization code.
	 */
	@Override
	public void robotInit() {

		//Create all of the choices
		autoChooser = new SendableChooser<>();
		teleopChooser = new SendableChooser<>();
		startingPositionChooser = new SendableChooser<>();
		destinationChooser = new SendableChooser<>();
		autoModeChooser = new SendableChooser<>();
		
		//Fill in commands for auto chooser
	/*	autoChooser.addObject("Test Grabber", new GrabberTest());
		autoChooser.addObject("Straight into Switch", new StraightSwitch());
		autoChooser.addObject("Curved Turn 90", new CurveDriveCommand(.2, .5, 90));
		autoChooser.addDefault("Turn 90", new TurnCommand(90));
		autoChooser.addObject("Turn 180", new TurnCommand(180));
		autoChooser.addObject("Turn 270", new TurnCommand(270));
		autoChooser.addObject("Turn 360", new TurnCommand(360));
		autoChooser.addObject("Turn -90", new TurnCommand(-90));
		autoChooser.addObject("Turn -180", new TurnCommand(-180));
		autoChooser.addObject("Turn 358", new TurnCommand(358));
		autoChooser.addObject("Right start Right switch", new RightStartRightSwitch());
		autoChooser.addObject("Right start Left switch", new RightStartLeftSwitch());
		autoChooser.addObject("Right start Right scale", new RightStartRightScale());
		autoChooser.addObject("Right start Left scale", new RightStartLeftScale());
		autoChooser.addObject("Left start Left switch", new LeftStartLeftSwitch());
		autoChooser.addObject("Left start Right switch", new LeftStartRightSwitch());
		autoChooser.addObject("Left start Left scale", new LeftStartLeftScale());
		autoChooser.addObject("Left start Right scale", new LeftStartRightScale());
		autoChooser.addObject("Center start Left switch", new CenterLeftSwitch());
		autoChooser.addObject("Center start Right switch", new CenterRightSwitch());
		autoChooser.addObject("Test Wheels", new TestWheelsCommand());
		autoChooser.addObject("Wave Servo", new WaveServoCommand());
		autoChooser.addObject("Drive 5ft", new DriveCommand(5 * 12, 0.5));
		autoChooser.addObject("Drive 10ft", new DriveCommand(10 * 12, 0.5));
		autoChooser.addObject("Drive 20ft", new DriveCommand(20 * 12, 0.5));
		autoChooser.addObject("Drive 40ft", new DriveCommand(40 * 12, 0.2));
		autoChooser.addObject("Upstairs Square", new UpstairsSquareCommand());
		autoChooser.addObject("Autonomous Right Start Switch", new RightSwitch());
		autoChooser.addObject("Lift test", new LiftTest());
//		autoChooser.addObject("Grabber Test", new GrabberTest()); */
		//All of our Teleop options
		autoChooser.addObject("Test Command", new TestCommand());
		teleopChooser.addDefault("Teleop Tank Drive", new XboxControllerArcadeDrive());
		teleopChooser.addObject("Ryan's Teleop Drive", new TeleopDriveCommand());
		teleopChooser.addObject("Anthony's Joystick Drive", new LogitechJoystickDrive());
		
		//Starting Positions should be left, right and center
		startingPositionChooser.addDefault("Right", StartingPosition.RIGHT);
		startingPositionChooser.addObject("Center", StartingPosition.CENTER);
		startingPositionChooser.addObject("Left", StartingPosition.LEFT);
		
		//What are we going for? The switch or the scale.
		destinationChooser.addDefault("Switch", Destination.SWITCH);
		destinationChooser.addObject("Scale", Destination.SCALE);
		
		//What mode are we going for.
		autoModeChooser.addDefault("Competition Auto", AutoMode.COMPETITION);
		autoModeChooser.addObject("Test Auto", AutoMode.TEST);
		
		SmartDashboard.putData("Test Auto", autoChooser);		
		SmartDashboard.putData("Teleop", teleopChooser);
		SmartDashboard.putData("Competition Auto - Starting Position", startingPositionChooser);
		SmartDashboard.putData("Competition Auto - Destination", destinationChooser);
		SmartDashboard.putData("Auto-Mode", autoModeChooser);
		
		CameraServer.getInstance().startAutomaticCapture();
	}

	/**
	 * This function is called once each time the robot enters Disabled mode. You
	 * can use it to reset any subsystem information you want to clear when the
	 * robot is disabled.
	 */
	@Override
	public void disabledInit() {

	}

	@Override
	public void disabledPeriodic() {
		Scheduler.getInstance().enable();
	}

	/**
	 * This autonomous (along with the chooser code above) shows how to select
	 * between different autonomous modes using the dashboard. The sendable chooser
	 * code works with the Java SmartDashboard. If you prefer the LabVIEW Dashboard,
	 * remove all of the chooser code and uncomment the getString code to get the
	 * auto name from the text box below the Gyro
	 *
	 * <p>
	 * You can add additional auto modes by adding additional commands to the
	 * chooser code above (like the commented example) or additional comparisons to
	 * the switch structure below with additional strings & commands.
	 */
	@Override
	public void autonomousInit() {
		//Close the grabber
		SS_GRABBER.getSolenoid().set(Value.kForward);
		
		System.out.println("autoModeChooser.getSelected(): " + autoModeChooser.getSelected());
		System.out.println("autoChooser.getSelected(): " + autoChooser.getSelected());
		System.out.println("autoModeChooser.getSelected(): " + autoModeChooser.getSelected());
		System.out.println("destinationChooser.getSelected(): " + destinationChooser.getSelected());
		System.out.println("startingPositionChooser.getSelected(): " + startingPositionChooser.getSelected());
		System.out.println("Game Specific Message: " + DriverStation.getInstance().getGameSpecificMessage());

		if(autoModeChooser.getSelected() == AutoMode.TEST) {
			opInit(autoChooser);
		}
		else if(autoModeChooser.getSelected() == AutoMode.COMPETITION){
			if(destinationChooser.getSelected() == Destination.SCALE) {
				if(startingPositionChooser.getSelected() == StartingPosition.CENTER) {
					opInit(new CenterBailOut());
					System.out.print("Incorrect Selection Robot Cannot go from Center to scale");
				}
				else if(startingPositionChooser.getSelected() == StartingPosition.LEFT) {
					if(getScaleConfig() == PlateConfiguration.LEFT) {
						opInit(new LeftStartLeftScale());
					}
					else if(getScaleConfig() == PlateConfiguration.RIGHT) {
						opInit(new LeftStartRightScale());
					}
					else {
						opInit(new SideBailOut());
					}
				}
				else if(startingPositionChooser.getSelected() == StartingPosition.RIGHT) {
					if(getScaleConfig() == PlateConfiguration.LEFT) {
						opInit(new RightStartLeftScale());
					}
					else if(getScaleConfig() == PlateConfiguration.RIGHT) {
						opInit(new RightStartRightScale());
					}
					else {
						opInit(new SideBailOut());
					}
				}
			}
			else if(destinationChooser.getSelected() == Destination.SWITCH) {
				if(startingPositionChooser.getSelected() == StartingPosition.CENTER) {
  					if(getNearSwitchConfig() == PlateConfiguration.LEFT) {
						opInit(new CenterLeftSwitch());
					}
					else if(getNearSwitchConfig() == PlateConfiguration.RIGHT) {
						opInit(new CenterRightSwitch());
					}
					else {
						opInit(new CenterBailOut());
					}
				}
				else if(startingPositionChooser.getSelected() == StartingPosition.LEFT) {
					if(getNearSwitchConfig() == PlateConfiguration.LEFT) {
						opInit(new LeftStartLeftSwitch());
					}
					else if(getNearSwitchConfig() == PlateConfiguration.RIGHT) {
						opInit(new LeftStartRightSwitch());
					}
					else {
						opInit(new SideBailOut());
					}
				}
				else if(startingPositionChooser.getSelected() == StartingPosition.RIGHT) {
					if(getNearSwitchConfig() == PlateConfiguration.LEFT) {
						opInit(new RightStartLeftSwitch());
					}
					else if(getNearSwitchConfig() == PlateConfiguration.RIGHT) {
						opInit(new RightStartRightSwitch());
					}
					else {
						opInit(new SideBailOut());
					}
				
				}
			}
		}
		
		System.out.println("Command to execute: " + currentCommand);
	}

	public static PlateConfiguration getNearSwitchConfig() {
		
		String info = null;
		
		try {
			//TODO - This is changed for Eagan. Change back before comp.
			 info = DriverStation.getInstance().getGameSpecificMessage();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		System.out.println(info);
		
		if(info == null || info.equals("") || info.length() != 3) {
			return PlateConfiguration.UNKNOWN;
			//checks for correct data 
		}
		info = info.toLowerCase();
		if (info.charAt(0) == 'r') {
			return PlateConfiguration.RIGHT;
		} else if (info.charAt(0) == 'l') {
			return PlateConfiguration.LEFT;
		} else {
			return PlateConfiguration.UNKNOWN;
			//makes sure it is l or r 
		}
	}

	public static PlateConfiguration getScaleConfig() {
		String info = DriverStation.getInstance().getGameSpecificMessage();
		if(info == null || info.equals("") || info.length() != 3) {
			return PlateConfiguration.UNKNOWN;
		}
		info = info.toLowerCase();
		if (info.charAt(1) == 'r') {
			return PlateConfiguration.RIGHT;
		} else if (info.charAt(1) == 'l') {
			return  PlateConfiguration.LEFT;
		} else {
			return PlateConfiguration.UNKNOWN;
		}
	}

	public static PlateConfiguration getFarSwitchConfig() {
		String info = DriverStation.getInstance().getGameSpecificMessage();
		if(info == null || info.equals("") || info.length() != 3) {
			return PlateConfiguration.UNKNOWN;
		}
		info = info.toLowerCase();
		if (info.charAt(2) == 'r') {
			return PlateConfiguration.RIGHT;
		} else if (info.charAt(2) == 'l') {
			return PlateConfiguration.LEFT;
		} else {
			return PlateConfiguration.UNKNOWN;
		}
	}

	

	/**
	 * This function is called periodically during autonomous.
	 */
	@Override
	public void autonomousPeriodic() {
		Scheduler.getInstance().run();
	}

	@Override
	public void teleopInit() {
		opInit(teleopChooser);
	}

	public void opInit(Command command) {
		if (currentCommand != null) {
			currentCommand.cancel();
		}

		currentCommand = command;

		if (currentCommand != null) {
			currentCommand.start();
		}
	}

	public void opInit(SendableChooser<Command> chooser) {
		opInit(chooser.getSelected());
	}

	/**
	 * This function is called periodically during operator control.
	 */
	@Override
	public void teleopPeriodic() {
		Scheduler.getInstance().run();
	}

	@Override
	public void testInit() {
		opInit(new TeleopDriveCommand());
	}

	/**
	 * This function is called periodically during test mode.
	 */
	@Override
	public void testPeriodic() {
		((TeleopDriveCommand) currentCommand).publicExecute();
	}
}
