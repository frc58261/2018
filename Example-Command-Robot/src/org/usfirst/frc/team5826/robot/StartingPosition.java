package org.usfirst.frc.team5826.robot;

public enum StartingPosition {
	LEFT,
	RIGHT,
	CENTER
}
