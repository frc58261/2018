package org.usfirst.frc.team5826.robot.commands.auto;

import edu.wpi.first.wpilibj.command.CommandGroup;
import org.usfirst.frc.team5826.robot.commands.DriveCommand;
import org.usfirst.frc.team5826.robot.commands.GrabberCommand;
import org.usfirst.frc.team5826.robot.commands.LiftCommand;
import org.usfirst.frc.team5826.robot.commands.TurnCommand;
import org.usfirst.frc.team5826.robot.commands.WaveServoCommand;

public class RightStartRightSwitch extends CommandGroup {	

	public RightStartRightSwitch() {
//		Example code
		addParallel(new LiftCommand (30));
		addSequential(new DriveCommand(150, .5));
		addSequential(new TurnCommand(getAngleDirection() * -90));
		addSequential(new DriveCommand(25, .5));
		addSequential(new DriveCommand(4, .4));
		addSequential(new GrabberCommand(true)); 
	}
	public double getAngleDirection() {
		return 1;
	}
	
}
