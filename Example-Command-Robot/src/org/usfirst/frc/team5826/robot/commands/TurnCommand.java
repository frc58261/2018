package org.usfirst.frc.team5826.robot.commands;

import org.usfirst.frc.team5826.robot.Robot;

import edu.wpi.first.wpilibj.ADXRS450_Gyro;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.interfaces.Gyro;

//TODO - Check this page out for ideas...
//http://first.wpi.edu/FRC/roborio/release/docs/java/edu/wpi/first/wpilibj/drive/DifferentialDrive.html#curvatureDrive-double-double-boolean-
public class TurnCommand extends Command {
	private double angleToTurn;
	private int autoHits;
	private Gyro gyro = Robot.SS_DRIVEBASE.getGyro();
	private SpeedControllerGroup leftDrive = Robot.SS_DRIVEBASE.getLeftDrive();
	private SpeedControllerGroup rightDrive = Robot.SS_DRIVEBASE.getRightDrive();
	private DifferentialDrive myRobot = new DifferentialDrive(leftDrive, rightDrive);

	// Called just before this Command runs the first time
	@Override
	protected void initialize() {
		autoHits = 0;
		gyro.reset();
	}

	public TurnCommand(double angleToTurn) {
		this.angleToTurn = angleToTurn;
		requires(Robot.SS_DRIVEBASE);
	}

	// Called repeatedly when this Command is scheduled to run
	@Override
	protected void execute() {
		double angleTraveled = gyro.getAngle();
		double factor=angleToTurn / Math.abs(angleToTurn);
		double speed =factor* Math.cos((Math.PI * angleTraveled) / (2 * angleToTurn));
		if (speed > 0) {
			speed = Math.max(0.2, speed);
			speed = Math.min(.4, speed);
		} else {
			speed = Math.min(-.2, speed);
			speed = Math.max(-.4, speed);
		}
		
		if (Math.abs(angleToTurn - angleTraveled) < .5) {
			autoHits++;

		}
		myRobot.curvatureDrive(0, speed, true);
	}

	// Make this return true when this Command no longer needs to run execute()
	@Override
	protected boolean isFinished() {
		return autoHits > 10;
	}

	// Called once after isFinished returns true
	@Override
	protected void end() {
		myRobot.arcadeDrive(0, 0);
		// No while loops
	}

	// Called when another command which requires one or more of the same
	// subsystems is scheduled to run
	@Override
	protected void interrupted() {
	}

}
