package org.usfirst.frc.team5826.robot.commands;

import org.usfirst.frc.team5826.robot.Robot;

import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.interfaces.Gyro;

public class DriveCommand extends Command {
	private double distanceInches;
	private Encoder leftEncoder = Robot.SS_DRIVEBASE.getLeftEncoder();
	private Encoder rightEncoder = Robot.SS_DRIVEBASE.getRightEncoder();
	private SpeedControllerGroup leftDrive = Robot.SS_DRIVEBASE.getLeftDrive();
	private SpeedControllerGroup rightDrive = Robot.SS_DRIVEBASE.getRightDrive();
	private DifferentialDrive myRobot = new DifferentialDrive(leftDrive, rightDrive);
	private double distanceTraveled;
	private int speedFactor;
	private double speed;
	private double newSpeed;
	private Gyro gyro = Robot.SS_DRIVEBASE.getGyro();

	// Called just before this Command runs the first time
	@Override
	protected void initialize() {
		// distanceTraveled = 0;
		// speed = 0;
		speedFactor = 0;
		leftEncoder.reset();
		rightEncoder.reset();
		gyro.reset();
	}

	public DriveCommand(double distanceInches, double speed) {
		this.distanceInches = distanceInches;
		this.speed = speed;
		requires(Robot.SS_DRIVEBASE);
	}

	// Called repeatedly when this Command is scheduled to run
	@Override
	protected void execute() {
		newSpeed = speed; 
		if (speedFactor++ < 25) {
			newSpeed = speed * (speedFactor * 0.04);
		} else if (distanceTraveled() + 10 > distanceInches) {
			newSpeed = speed * 0.25;
		}
		myRobot.curvatureDrive(newSpeed, Straighten(), false);

	}

	// Make this return true when this Command no longer needs to run execute()
	@Override
	protected boolean isFinished() {
		// System.out.println(distanceTraveled());
		return distanceTraveled() > distanceInches;
	}

	// Called once after isFinished returns true
	@Override
	protected void end() {
		myRobot.arcadeDrive(0, 0);
		// No while loops
	}

	// Called when another command which requires one or more of the same
	// subsystems is scheduled to run
	@Override
	protected void interrupted() {
	}

	/*
	 * public double Straighten() { double a, b, value; a =
	 * leftEncoder.getDistance(); b = rightEncoder.getDistance(); value = (a + b) /
	 * -25; // competition bot 2017 // value = (a+b)/200; //practice bot
	 * //System.out.println(value); return value; }
	 */
	public double Straighten() {
		double value;
		value = gyro.getAngle() * -.5;
		// System.out.println(value);
		value = Math.min(value, .1);
		return Math.max(value, -.1);

	}

	public double distanceTraveled() {
		double distanceIn;
		// int count = 0;
		distanceIn = leftEncoder.getDistance() / 11.2;
		return distanceIn;

	}
}
