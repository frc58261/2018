package org.usfirst.frc.team5826.robot.commands;

import org.usfirst.frc.team5826.robot.Robot;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;

public class GrabberTest extends Command {

	private boolean done = false;
	public GrabberTest() {
		requires(Robot.SS_GRABBER);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void initialize() {
	
	}

	@Override
	protected void execute() {
		
		DoubleSolenoid grabber = Robot.SS_GRABBER.getSolenoid();

		grabber.set(Value.kForward);
		Timer.delay(1);
		grabber.set(Value.kReverse);
		done = true;
	}
	
	@Override
	protected boolean isFinished() {
		// TODO Auto-generated method stub
//		System.out.println(testServo.getAngle());
		return done;
	}
}
