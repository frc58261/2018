package org.usfirst.frc.team5826.robot.commands;

import org.usfirst.frc.team5826.robot.Robot;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;

public class LiftCommand extends Command {
	private boolean done = false;
	private double height = 0;
	

	public LiftCommand(double height) { 
		requires(Robot.SS_LIFT);
		this.height = height;
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void initialize() {

	}

	@Override
	protected void execute() {
		if (Math.abs(Robot.SS_LIFT.currentHeight()- height) > .25 && Robot.SS_LIFT.currentHeight() < height){
			Robot.SS_LIFT.raise();
		}
		else if(Math.abs(Robot.SS_LIFT.currentHeight()- height) > .25 && Robot.SS_LIFT.currentHeight() > height){
			Robot.SS_LIFT.lower();
		}
		else {
			Robot.SS_LIFT.stop();
			done = true;
		}
	}

	@Override
	protected boolean isFinished() {
		// TODO Auto-generated method stub
		// System.out.println(testServo.getAngle());
		return done;
	}
}
