package org.usfirst.frc.team5826.robot.commands.auto;

public class CenterRightSwitch extends CenterLeftSwitch {

	@Override
	public double getAngleDirection() {
		return -1;
	}
}
