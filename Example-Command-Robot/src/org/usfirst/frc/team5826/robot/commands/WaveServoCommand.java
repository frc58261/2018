package org.usfirst.frc.team5826.robot.commands;

import org.usfirst.frc.team5826.robot.Robot;

import edu.wpi.first.wpilibj.Servo;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;

public class WaveServoCommand extends Command {
	private Servo testServo = Robot.SS_LIFT.getTestServo();
	
	@Override
	protected void initialize() {
		testServo.set(-90);
		Timer.delay(1);
	}
	
	public WaveServoCommand() {
		requires(Robot.SS_LIFT);
	}

	@Override
	protected void execute() {
		
		testServo.set(90);

	}
	
	@Override
	protected boolean isFinished() {
		// TODO Auto-generated method stub
//		System.out.println(testServo.getAngle());
		return testServo.getAngle() == 90;
	}

}
