package org.usfirst.frc.team5826.robot.commands;

import edu.wpi.first.wpilibj.command.CommandGroup;

public class UpstairsSquareCommand extends CommandGroup {

	public UpstairsSquareCommand() {
		addSequential(new DriveCommand(20 * 12, .6));
		addSequential(new PauseCommand(1));
		addSequential(new TurnCommand(90));
		addSequential(new DriveCommand(142, .6));
		addSequential(new PauseCommand(1));
		addSequential(new TurnCommand(90));
		addSequential(new DriveCommand(20 * 12, .6));
		addSequential(new PauseCommand(1));
		addSequential(new TurnCommand(90));
		addSequential(new DriveCommand(142, .6));
		addSequential(new PauseCommand(1));
		addSequential(new TurnCommand(90));
	}

	
}
