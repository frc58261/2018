package org.usfirst.frc.team5826.robot.commands;

import org.usfirst.frc.team5826.robot.Robot;
import org.usfirst.frc.team5826.robot.subsystems.LiftSubsystem;

import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.Talon;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;

public class XboxControllerArcadeDrive extends Command {
	private SpeedControllerGroup leftDrive = Robot.SS_DRIVEBASE.getLeftDrive();
	private SpeedControllerGroup rightDrive = Robot.SS_DRIVEBASE.getRightDrive();
	private DifferentialDrive drive = new DifferentialDrive(leftDrive, rightDrive);
//	Talon lift = new Talon(1);
	private boolean rightBumper;
	private boolean leftBumper;
	private boolean aButton;
	private boolean bButton;
	private boolean yButton;
	private boolean xButton;
	private boolean startButton;
	private boolean backButton;
	public XboxControllerArcadeDrive() {
		requires(Robot.SS_DRIVEBASE);
		requires(Robot.SS_LIFT);
		requires(Robot.SS_GRABBER);
		requires(Robot.SS_CLIMB);
	
	}
	protected void initialize() {
		aButton=false;
		bButton=false;
		rightBumper=false;
		leftBumper=false;
		yButton=false;
		xButton=false;
		startButton=false;
		backButton = false;
	}
	public void publicExecute() {
		execute();
		
		
	}
	
	protected void execute() {
		double throttle = 1-(Robot.oi.getXbox().getTriggerAxis(Hand.kLeft)*.5);
		double liftThrottle = (1 - ((Math.max(50, Robot.SS_LIFT.currentHeight()) - 50)) / 60);
		

		if(Robot.oi.getXbox().getAButtonPressed()) {
			Robot.SS_GRABBER.getSolenoid().set(Value.kForward);
		}
		if(Robot.oi.getXbox().getBButtonPressed()) {
			Robot.SS_GRABBER.getSolenoid().set(Value.kReverse);		
		}
		if(Robot.oi.getXbox().getYButtonPressed()) {
			yButton=true;
//			System.out.println("Button Pressed");
		}
		if(Robot.oi.getXbox().getYButtonReleased()) {
			yButton=false;
//	System.out.println("Button Released");
		}
		if(yButton==true) {
			Robot.SS_LIFT.raise();
//			System.out.println("Button True");
		}
		if(yButton==false && xButton == false) {
			Robot.SS_LIFT.stop();
//			System.out.println("Button False");
		}
		if(Robot.oi.getXbox().getXButtonPressed()) {
			xButton=true;
//			System.out.println("Button Pressed");
		}
		if(Robot.oi.getXbox().getXButtonReleased()) {
			xButton=false;
//			System.out.println("Button Released");
		}
		if(xButton==true) {
			Robot.SS_LIFT.lower();
//			System.out.println("Button True");
		}
	
		
		if(Robot.oi.getXbox().getBumperPressed(Hand.kRight)) {
			rightBumper = true;
		}
		if(Robot.oi.getXbox().getBumperReleased(Hand.kRight)) {
			rightBumper = false;
		}
		if(Robot.oi.getXbox().getBumperPressed(Hand.kLeft)) {
			leftBumper = true;
		}
		if(Robot.oi.getXbox().getBumperReleased(Hand.kLeft)) {
			leftBumper = false;
		}
		if(Robot.oi.getXbox().getStartButtonPressed()) {
			startButton = true;
		}
		if(Robot.oi.getXbox().getStartButtonReleased()) {
			startButton = false;
		}
		if(Robot.oi.getXbox().getBackButtonPressed()) {
			backButton = true;
		}
		if(Robot.oi.getXbox().getBackButtonReleased()) {
			backButton = false;
		}
	
//		if (bumperPressed) {
//			drive.arcadeDrive(-Robot.oi.getXbox().getY(Hand.kRight), Robot.oi.getXbox().getX(Hand.kRight));
			//lift.set(0.1 * Robot.oi.getXbox().getX(Hand.kLeft));
//		}
		if(rightBumper && startButton) {
			Robot.SS_CLIMB.climbUp();
		}
		
		else if(rightBumper && backButton) {
			Robot.SS_CLIMB.climbDown();
		}
		else {
			Robot.SS_CLIMB.stop();
		}
		
		if(leftBumper && startButton) {
			Robot.SS_LIFT.unlock();
		}
		
		if(leftBumper && backButton) {
			Robot.SS_LIFT.getliftEncoder().reset();
		}
		
		drive.arcadeDrive(-Robot.oi.getXbox().getY(Hand.kLeft) * throttle * liftThrottle, Robot.oi.getXbox().getX(Hand.kLeft) * throttle * liftThrottle);
		
	
		}
		//	System.out.println(throttle);
		//	System.out.println("POV" + Robot.oi.getXbox().getPOV());
	


	public XboxControllerArcadeDrive(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	public XboxControllerArcadeDrive(double timeout) {
		super(timeout);
		// TODO Auto-generated constructor stub
	}

	public XboxControllerArcadeDrive(String name, double timeout) {
		super(name, timeout);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected boolean isFinished() {
		// TODO Auto-generated method stub
		return false;
	}

}
