package org.usfirst.frc.team5826.robot.commands.auto;

import org.usfirst.frc.team5826.robot.commands.LiftCommand;
import org.usfirst.frc.team5826.robot.commands.PauseCommand;

import edu.wpi.first.wpilibj.command.CommandGroup;

public class LiftTest extends CommandGroup {

	public LiftTest() {
		addSequential(new LiftCommand(50));
		addSequential(new PauseCommand(2));
		addSequential(new LiftCommand(10));
		addSequential(new PauseCommand(2));
		addSequential(new LiftCommand(0));
	}

	public LiftTest(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

}
