package org.usfirst.frc.team5826.robot.commands.auto;

public class LeftStartRightScale extends RightStartLeftScale {

	@Override
	public double getAngleDirection() {
		return -1;
	}

}
