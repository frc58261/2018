package org.usfirst.frc.team5826.robot.commands;

import org.usfirst.frc.team5826.robot.Robot;
import org.usfirst.frc.team5826.robot.subsystems.LiftSubsystem;

import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.Talon;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;

public class RyansXboxDrive extends Command {
	private SpeedControllerGroup leftDrive = Robot.SS_DRIVEBASE.getLeftDrive();
	private SpeedControllerGroup rightDrive = Robot.SS_DRIVEBASE.getRightDrive();
	private DifferentialDrive drive = new DifferentialDrive(leftDrive, rightDrive);
	
	public RyansXboxDrive() {
		requires(Robot.SS_DRIVEBASE);
		requires(Robot.SS_LIFT);
	
	}
	protected void initialize() {
		
	}
	public void publicExecute() {
		execute();
		
		
	}
	
	protected void execute() {
		double forward = Robot.oi.getXbox().getTriggerAxis(Hand.kRight);
		double backward = Robot.oi.getXbox().getTriggerAxis(Hand.kLeft);
		double turn = Robot.oi.getXbox().getY(Hand.kRight);
		
		if(forward > 0) {
			drive.arcadeDrive(forward, turn);
		}
		else if(backward > 0) {
			drive.arcadeDrive(-1 * backward, turn);
		}
	}

	public RyansXboxDrive(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	public RyansXboxDrive(double timeout) {
		super(timeout);
		// TODO Auto-generated constructor stub
	}

	public RyansXboxDrive(String name, double timeout) {
		super(name, timeout);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected boolean isFinished() {
		// TODO Auto-generated method stub
		return false;
	}

}
