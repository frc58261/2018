package org.usfirst.frc.team5826.robot.commands.auto;

import edu.wpi.first.wpilibj.command.CommandGroup;
import org.usfirst.frc.team5826.robot.commands.DriveCommand;
import org.usfirst.frc.team5826.robot.commands.GrabberCommand;
import org.usfirst.frc.team5826.robot.commands.LiftCommand;
import org.usfirst.frc.team5826.robot.commands.TurnCommand;
import org.usfirst.frc.team5826.robot.commands.WaveServoCommand;

public class RightStartLeftScale extends CommandGroup {

	public RightStartLeftScale() {
//		Example code
		addParallel(new LiftCommand(5));
		addSequential(new DriveCommand(17*12, .6));
		addSequential(new TurnCommand(getAngleDirection() * -90));	
		addParallel(new LiftCommand(30));
		addSequential(new DriveCommand(15.5*12, .6));
		addSequential(new DriveCommand(0.5, 0.4));
		addParallel(new LiftCommand(80));
		addSequential(new TurnCommand(getAngleDirection() * 90));
		addSequential(new DriveCommand(42, .5));
		addSequential(new GrabberCommand(true));
	}
	
	public double getAngleDirection() {
		return 1;
	}

}
