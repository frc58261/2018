package org.usfirst.frc.team5826.robot.commands.master;

import org.usfirst.frc.team5826.robot.PlateConfiguration;
import org.usfirst.frc.team5826.robot.Robot;
import org.usfirst.frc.team5826.robot.commands.DriveCommand;
import org.usfirst.frc.team5826.robot.commands.auto.RightStartLeftSwitch;
import org.usfirst.frc.team5826.robot.commands.auto.RightStartRightSwitch;

import edu.wpi.first.wpilibj.command.CommandGroup;

public class RightSwitch extends CommandGroup {
	// Can be overwritten by teams
	protected void initialize() {
		if(Robot.getNearSwitchConfig() == PlateConfiguration.RIGHT) {
			addSequential(new RightStartRightSwitch());
		}
		else if(Robot.getNearSwitchConfig() == PlateConfiguration.LEFT) {
			addSequential(new RightStartLeftSwitch());
		}
		else {
			addSequential(new DriveCommand(14, .4));
		}
	}

}
