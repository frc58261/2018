package org.usfirst.frc.team5826.robot.commands.auto;

import org.usfirst.frc.team5826.robot.commands.DriveCommand;
import org.usfirst.frc.team5826.robot.commands.PauseCommand;
import org.usfirst.frc.team5826.robot.commands.TurnCommand;

import edu.wpi.first.wpilibj.command.CommandGroup;

public class CenterBailOut extends CommandGroup {

	public CenterBailOut() {
		addSequential(new DriveCommand(3*12, .5));
		addSequential(new TurnCommand(-90));
		addSequential(new PauseCommand(2));
		addSequential(new DriveCommand(10*12, .5));
		addSequential(new TurnCommand(90));
		addSequential(new DriveCommand(8*12, .5));
	}

	

}
