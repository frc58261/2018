package org.usfirst.frc.team5826.robot.commands.auto;
import org.usfirst.frc.team5826.robot.commands.LiftCommand;
import org.usfirst.frc.team5826.robot.commands.DriveCommand;
import org.usfirst.frc.team5826.robot.commands.TurnCommand;
import org.usfirst.frc.team5826.robot.commands.WaveServoCommand;
import org.usfirst.frc.team5826.robot.commands.GrabberCommand;

import edu.wpi.first.wpilibj.command.CommandGroup;

public class CenterLeftSwitch extends CommandGroup {

	public CenterLeftSwitch() {

		addParallel(new LiftCommand(30));
		addSequential(new DriveCommand(12, .5));
		addSequential(new TurnCommand(getAngleDirection() * -38));
		addSequential(new DriveCommand(98.4, .5));
		addSequential(new TurnCommand(getAngleDirection() * 38));
		addSequential(new DriveCommand(5, .5));
		addSequential(new GrabberCommand(true));
	}
	public double getAngleDirection() {
		return 1;
	}
}
