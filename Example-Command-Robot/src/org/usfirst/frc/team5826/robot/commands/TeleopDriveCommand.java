package org.usfirst.frc.team5826.robot.commands;

import org.usfirst.frc.team5826.robot.Robot;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;

public class TeleopDriveCommand extends Command {
	private SpeedControllerGroup leftDrive = Robot.SS_DRIVEBASE.getLeftDrive();
	private SpeedControllerGroup rightDrive = Robot.SS_DRIVEBASE.getRightDrive();
	private DifferentialDrive drive = new DifferentialDrive(leftDrive, rightDrive);
	
	public TeleopDriveCommand() {
		requires(Robot.SS_DRIVEBASE);
	}
	
	// Called just before this Command runs the first time
	@Override
	protected void initialize() {
	
	}

	public void publicExecute() {
		execute();
	}
	
	// Called repeatedly when this Command is scheduled to run
	@Override
	protected void execute() {
		double throttle = (Robot.oi.getJoystick().getThrottle()+1.8)/2;

		drive.arcadeDrive( throttle * -Robot.oi.getJoystick().getY(), throttle * Robot.oi.getJoystick().getX());
		
	}

	// Make this return true when this Command no longer needs to run execute()
	@Override
	protected boolean isFinished() {
		return false;
	}

	// Called once after isFinished returns true
	@Override
	protected void end() {
		//No while loops
	}

	// Called when another command which requires one or more of the same
	// subsystems is scheduled to run
	@Override
	protected void interrupted() {
		System.out.println("Interupted");
	}

}
