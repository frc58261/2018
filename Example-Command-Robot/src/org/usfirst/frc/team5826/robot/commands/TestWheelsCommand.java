package org.usfirst.frc.team5826.robot.commands;

import org.usfirst.frc.team5826.robot.Robot;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;

public class TestWheelsCommand extends Command {
	private Timer timer = new Timer();
	
	public TestWheelsCommand() {
		requires(Robot.SS_DRIVEBASE);
	}
	
	@Override
	protected void execute() {
		
		Robot.SS_DRIVEBASE.getLeftback().set(.5);
		timer.delay(2);
		Robot.SS_DRIVEBASE.getLeftback().set(0);
		
		Robot.SS_DRIVEBASE.getLeftfront().set(.5);
		timer.delay(2);
		Robot.SS_DRIVEBASE.getLeftfront().set(0);
		
		Robot.SS_DRIVEBASE.getRightback().set(.5);
		timer.delay(2);
		Robot.SS_DRIVEBASE.getRightback().set(0);
		
		Robot.SS_DRIVEBASE.getRightfront().set(.5);
		timer.delay(2);
		Robot.SS_DRIVEBASE.getRightfront().set(0);
		

		
	}
	
	@Override
	protected boolean isFinished() {
		// TODO Auto-generated method stub
		return false;
	}

}
