package org.usfirst.frc.team5826.robot.commands;

import edu.wpi.first.wpilibj.command.CommandGroup;

public class CircleCommand extends CommandGroup {

	public CircleCommand() {
		for(int i = 0; i < 4; i++) {
	//		addSequential(new DriveCommand(5 * 12));
			addSequential(new TurnCommand(90));
		}	
	}

}
