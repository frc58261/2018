package org.usfirst.frc.team5826.robot.commands;

import org.usfirst.frc.team5826.robot.Robot;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;

public class GrabberCommand extends Command {
	private boolean done = false;
	private boolean open = false;

	public GrabberCommand(boolean open) {//false closes and true opens
		requires(Robot.SS_GRABBER);
		this.open = open;
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void initialize() {

	}

	@Override
	protected void execute() {

		DoubleSolenoid grab = Robot.SS_GRABBER.getSolenoid();
		if (!open) {
			grab.set(Value.kForward); //forward closes
		} else {
			grab.set(Value.kReverse);//reverse opens
		}

		done = true;
	}

	@Override
	protected boolean isFinished() {
		// TODO Auto-generated method stub
		// System.out.println(testServo.getAngle());
		return done;
	}
}
