package org.usfirst.frc.team5826.robot.commands.auto;

import org.usfirst.frc.team5826.robot.commands.DriveCommand;
import org.usfirst.frc.team5826.robot.commands.WaveServoCommand;

import edu.wpi.first.wpilibj.command.CommandGroup;

public class StraightSwitch extends CommandGroup {

	public StraightSwitch() {
		addSequential(new DriveCommand(106, .4));
		addSequential(new WaveServoCommand());
		
		// TODO Auto-generated constructor stub
	}

}
