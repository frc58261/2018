package org.usfirst.frc.team5826.robot.commands;

import org.usfirst.frc.team5826.robot.Robot;

import edu.wpi.first.wpilibj.command.CommandGroup;

public class TestCommand extends CommandGroup {

	public TestCommand() {
		addSequential(new DriveCommand(3 * 12, .4));
		addSequential(new TurnCommand(90));
		addSequential(new DriveCommand(3 * 12, .4));
		addSequential(new TurnCommand(180));
		addSequential(new DriveCommand(3 * 12, .4));
		addSequential(new TurnCommand(-90));
		addSequential(new DriveCommand(3 * 12, .4));
		addSequential(new TurnCommand(-180));
		
		

		
	}

}
