package org.usfirst.frc.team5826.robot.commands;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;

public class PauseCommand extends Command {
	private double waitTime; 
	private Timer timer;
	
	protected void initialize() {
		timer = new Timer();
		timer.start();
	}
	
	public PauseCommand(double waitTime) {
		this.waitTime = waitTime;
	}
	@Override
	protected boolean isFinished() {
		
		return timer.get() >= waitTime ;
	}

}
