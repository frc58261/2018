package org.usfirst.frc.team5826.robot.commands;

import org.usfirst.frc.team5826.robot.Robot;
import org.usfirst.frc.team5826.robot.subsystems.LiftSubsystem;

import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.Talon;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;

public class LogitechJoystickDrive extends Command {
	private SpeedControllerGroup leftDrive = Robot.SS_DRIVEBASE.getLeftDrive();
	private SpeedControllerGroup rightDrive = Robot.SS_DRIVEBASE.getRightDrive();
	private DifferentialDrive drive = new DifferentialDrive(leftDrive, rightDrive);

	private boolean Button6;
	private boolean Button5;
	private boolean Button3;
	private boolean Button11;
	private boolean Button12;
	private boolean POVUp;
	private boolean POVDown;
	private boolean trigger;
	private boolean Button8;
	private boolean Button7;
	

	public LogitechJoystickDrive() {
		requires(Robot.SS_DRIVEBASE);
		requires(Robot.SS_LIFT);
		requires(Robot.SS_GRABBER);

	}

	protected void initialize() {

		Button6 = false;
		Button5 = false;
		Button3 = false;
		Button11 = false;
		Button12 = false;
		POVDown = false;
		POVUp = false;
		trigger = false;
		Button8 = false;
		Button7 = false;
	}

	public void publicExecute() {
		execute();

	}

	protected void execute() {

		double throttle =  (Robot.oi.getJoystick().getThrottle() + 1.8) / 2;
		double liftThrottle = (1 - ((Math.max(50, Robot.SS_LIFT.currentHeight()) - 50)) / 60);
		if ((Robot.oi.getJoystick().getPOV() >= 0 && Robot.oi.getJoystick().getPOV() <= 45)
				|| (Robot.oi.getJoystick().getPOV() >= 315 && Robot.oi.getJoystick().getPOV() <= 360)) {
			POVUp = true;
		} else {
			POVUp = false;
		}
		if (Robot.oi.getJoystick().getPOV() >= 135 && Robot.oi.getJoystick().getPOV() <= 225) {
			POVDown = true;
		} else {
			POVDown = false;
		}

		if (Robot.oi.getJoystick().getRawButtonPressed(3)) {
			Robot.SS_GRABBER.getSolenoid().set(Value.kReverse);
		}
		if (Robot.oi.getJoystick().getRawButtonPressed(4)) {
			Robot.SS_GRABBER.getSolenoid().set(Value.kForward);
		}

		if (Robot.oi.getJoystick().getRawButtonPressed(5)) {
			Button5 = true;
		}
		if (Robot.oi.getJoystick().getRawButtonReleased(5)) {
			Button5 = false;
		}
		if (POVUp == true) {
			Robot.SS_LIFT.raise();
		}
		if (!POVUp && !POVDown && !Button8) {
			Robot.SS_LIFT.stop();
		}

		if (Robot.oi.getJoystick().getRawButtonPressed(3)) {
			Button3 = true;
		}
		if (Robot.oi.getJoystick().getRawButtonReleased(3)) {
			Button3 = false;
		}
		if (POVDown == true) {
			Robot.SS_LIFT.lower();
		}

		if (Robot.oi.getJoystick().getRawButtonPressed(6)) {
			Button6 = true;
		}
		if (Robot.oi.getJoystick().getRawButtonReleased(6)) {
			Button6 = false;
		}

		if (Robot.oi.getJoystick().getRawButtonPressed(12)) {
			Button12 = true;
		}
		if (Robot.oi.getJoystick().getRawButtonReleased(12)) {
			Button12 = false;
		}
		if (Robot.oi.getJoystick().getTriggerPressed()) {
			trigger = true;
		}
		if (Robot.oi.getJoystick().getTriggerReleased()) {
			trigger = false;
		}

		if (Robot.oi.getJoystick().getRawButtonPressed(11)) {
			Button11 = true;
		}
		if (Robot.oi.getJoystick().getRawButtonReleased(11)) {
			Button11 = false;
		}
		if (Button11 && trigger) {
			Robot.SS_CLIMB.climbDown();
		}
		else if (Button12 && trigger) {
			Robot.SS_CLIMB.climbUp();
		}
		else {
			Robot.SS_CLIMB.stop();
		}
		
		if (Robot.oi.getJoystick().getRawButtonPressed(8)) {
			Button8 = true;
		}
		if (Robot.oi.getJoystick().getRawButtonReleased(8)) {
			Button8 = false;
		}
		if (Robot.oi.getJoystick().getRawButtonPressed(7)) {
			Button7 = true;
		}
		if (Robot.oi.getJoystick().getRawButtonReleased(7)) {
			Button7 = false;
		}
		if (Button8) {
			Robot.SS_LIFT.unlock();
		}
		if (Button7) {
			Robot.SS_LIFT.getliftEncoder().reset();
		}
		

		if (Button6) {
			drive.curvatureDrive(throttle * -Robot.oi.getJoystick().getY() * liftThrottle,
					throttle * Robot.oi.getJoystick().getX() * liftThrottle, false);
		} else {
			drive.arcadeDrive(throttle * -Robot.oi.getJoystick().getY() * liftThrottle,
					throttle * Robot.oi.getJoystick().getX() * liftThrottle);
		}
	}

	public LogitechJoystickDrive(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	public LogitechJoystickDrive(double timeout) {
		super(timeout);
		// TODO Auto-generated constructor stub
	}

	public LogitechJoystickDrive(String name, double timeout) {
		super(name, timeout);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected boolean isFinished() {
		// TODO Auto-generated method stub
		return false;
	}

}
