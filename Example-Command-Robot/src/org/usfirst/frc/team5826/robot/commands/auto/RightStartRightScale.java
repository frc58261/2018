package org.usfirst.frc.team5826.robot.commands.auto;

import org.usfirst.frc.team5826.robot.commands.DriveCommand;
import org.usfirst.frc.team5826.robot.commands.GrabberCommand;
import org.usfirst.frc.team5826.robot.commands.LiftCommand;
import org.usfirst.frc.team5826.robot.commands.TurnCommand;
import org.usfirst.frc.team5826.robot.commands.WaveServoCommand;

import edu.wpi.first.wpilibj.command.CommandGroup;

public class RightStartRightScale extends CommandGroup {

	public RightStartRightScale() {
//		Example code
		addParallel(new LiftCommand(80));
		addSequential(new DriveCommand(25*12, .5));
		addSequential(new TurnCommand(getAngleDirection() * -90));
		addSequential(new DriveCommand(9, .5));
		addSequential(new DriveCommand(5, .4));
		addSequential(new GrabberCommand(true));
	}
	public double getAngleDirection() {
		return 1;
	}
}
