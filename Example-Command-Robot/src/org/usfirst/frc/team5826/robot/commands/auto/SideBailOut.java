package org.usfirst.frc.team5826.robot.commands.auto;

import org.usfirst.frc.team5826.robot.commands.DriveCommand;

import edu.wpi.first.wpilibj.command.CommandGroup;

public class SideBailOut extends CommandGroup {

	public SideBailOut() {
		addSequential(new DriveCommand(14*12, .4));
	}
}
