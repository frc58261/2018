/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package org.usfirst.frc.team5826.robot;

import org.usfirst.frc.team5826.robot.commands.TurnCommand;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.buttons.Button;
import edu.wpi.first.wpilibj.buttons.JoystickButton;

/**
 * This class is the glue that binds the controls on the physical operator
 * interface to the commands and command groups that allow control of the robot.
 */
public class OI {

	private Joystick joystick;
	private XboxController xboxController;
	public OI() {
		joystick = new Joystick(RobotMap.joystickPort);
		xboxController = new XboxController(RobotMap.xboxPort);
		
//		//When you hit a button, spin the robot around
//		Button aButton = new JoystickButton(joystick, 8);
//		aButton.whenPressed(new TurnCommand(180));
		
	}

	public Joystick getJoystick() {
		return joystick;
	}
	public XboxController getXbox() {
		return xboxController;
	}
}
