package org.usfirst.frc.team5826.robot;

public enum PlateConfiguration {
	RIGHT,
	LEFT,
	UNKNOWN
}
