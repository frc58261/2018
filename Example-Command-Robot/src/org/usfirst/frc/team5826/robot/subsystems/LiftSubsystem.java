package org.usfirst.frc.team5826.robot.subsystems;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.Servo;
import edu.wpi.first.wpilibj.Talon;
import edu.wpi.first.wpilibj.command.Subsystem;

//We'll probably need to control a list this year
public class LiftSubsystem extends Subsystem {

	private static final double MAX_HEIGHT = 80;
	private static final double MIN_HEIGHT = 0;
	private Talon lift = new Talon(1);
	Encoder liftEncoder = new Encoder(4,5);
	private Servo TestServo = new Servo(0);

	/**
	 * @return the testServo
	 */
	public Servo getTestServo() {
		return TestServo;
	}
	public DoubleSolenoid getSolenoid() {
		return liftBrake;
	}

	DoubleSolenoid liftBrake = new DoubleSolenoid(6, 2, 3); //2 is open, 3 is close
	/**
	 * @param testServo the testServo to set
	 */
//	public void setTestServo(Servo testServo) {
//		TestServo = testServo;
//	}
	
	@Override
	protected void initDefaultCommand() { 
		
	}
	public LiftSubsystem() {
		liftEncoder.reset();
	}

	public Encoder getliftEncoder() {
		return liftEncoder;
	}
	
	public void lock() {
		liftBrake.set(Value.kReverse);
	}
	
	public void unlock() {
		liftBrake.set(Value.kForward);
	}
	
	public boolean raise() {
		if (currentHeight() < MAX_HEIGHT) {
			unlock();
			lift.set(1); //1
			
			return true;
		} else {
			stop();
		
			return false;
		}
	}

	public boolean lower() {
		if (currentHeight() > MIN_HEIGHT) {
			unlock();
			lift.set(-0.5); //-.5
			
			return true;
		}

		else {
			stop();
			return false;
		}
	}
	

//	public void set(double height) {
//		if (currentHeight() < height) {
//			while (currentHeight() < height) {
//				raise();
//			}
//			while (currentHeight() > height) {
//				lower();
//			}
//		}
//
//		lift.set(0);
//
//	}

	public double currentHeight() {
		return liftEncoder.getDistance() / -34;
	}

	public void stop() {
		//liftBrake.set(Value.kForward);
		lift.set(0);
		lock();
	}

	// public Talon getLift() {
	// return lift;
	// }

}
