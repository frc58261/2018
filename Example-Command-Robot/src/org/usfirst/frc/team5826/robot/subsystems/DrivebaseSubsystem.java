package org.usfirst.frc.team5826.robot.subsystems;

import org.usfirst.frc.team5826.robot.Fixed_TalonSRX;
import org.usfirst.frc.team5826.robot.TestTalon;

import edu.wpi.first.wpilibj.ADXRS450_Gyro;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.interfaces.Gyro;

public class DrivebaseSubsystem extends Subsystem {
	TestTalon leftfront = new TestTalon(4);
	TestTalon leftback = new TestTalon(3);
	Encoder leftEncoder = new Encoder(0, 1);
	//left
	TestTalon rightfront = new TestTalon(2);
	TestTalon rightback = new TestTalon(1);
	Encoder rightEncoder = new Encoder(2, 3);
	//right
	SpeedControllerGroup leftDrive = new SpeedControllerGroup(leftfront, leftback);
	SpeedControllerGroup rightDrive = new SpeedControllerGroup(rightfront, rightback);
	//left side & right side
	private Gyro gyro = new ADXRS450_Gyro();
	
	public DrivebaseSubsystem() {
		
	}
	
	@Override
	protected void initDefaultCommand() {
		
	}
	
	public Gyro getGyro() {
		return gyro;
	}

	public TestTalon getLeftfront() {
		return leftfront;
	}

	public TestTalon getLeftback() {
		return leftback;
	}

	public Encoder getLeftEncoder() {
		return leftEncoder;
	}

	public TestTalon getRightfront() {
		return rightfront;
	}

	public TestTalon getRightback() {
		return rightback;
	}

	public Encoder getRightEncoder() {
		return rightEncoder;
	}

	public SpeedControllerGroup getLeftDrive() {
		return leftDrive;
	}

	public SpeedControllerGroup getRightDrive() {
		return rightDrive;
	}

}
