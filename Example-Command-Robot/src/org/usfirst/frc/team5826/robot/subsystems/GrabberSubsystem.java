package org.usfirst.frc.team5826.robot.subsystems;


import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.command.Subsystem;

public class GrabberSubsystem extends Subsystem{


	/**
	 * @return the grab
	 */
	public DoubleSolenoid getSolenoid() {
		return grab;
	}

	DoubleSolenoid grab = new DoubleSolenoid(6, 0, 1); //0 is open, 1 is close

	
	
	public GrabberSubsystem() {
		grab.set(Value.kForward);
	
	}

	@Override
	protected void initDefaultCommand() {
		// TODO Auto-generated method stub
		
	}

}
