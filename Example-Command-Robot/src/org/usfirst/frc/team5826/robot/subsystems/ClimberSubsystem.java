package org.usfirst.frc.team5826.robot.subsystems;

import edu.wpi.first.wpilibj.Talon;
import edu.wpi.first.wpilibj.command.Subsystem;

public class ClimberSubsystem extends Subsystem {

	public ClimberSubsystem() {
		// TODO Auto-generated constructor stub
	}
	Talon climb = new Talon(2);
	
	public void climbUp() {
		climb.set(1);
	}
	public void climbDown() {
		climb.set(-1);
	}
	public void stop() {
		climb.set(0);
	}
	
	
	@Override
	protected void initDefaultCommand() {
		// TODO Auto-generated method stub
		
	}

}
