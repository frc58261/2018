package org.usfirst.frc.team5826.robot;

import edu.wpi.first.wpilibj.ADXRS450_Gyro;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.Talon;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.interfaces.Gyro;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;

/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the IterativeRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the manifest file in the resource
 * directory.
 */
public class Robot extends IterativeRobot {
	Talon leftfront = new Talon(0);
	Talon leftback = new Talon(1);
	Encoder leftEncoder = new Encoder(0, 1);

	Talon rightfront = new Talon(2);
	Talon rightback = new Talon(3);
	Encoder rightEncoder = new Encoder(2, 3);
	double speedFactor = 1;
	boolean autocomplete = false;
	double turnSpeedFactor = 0; // defaults to no turn

	Joystick stick = new Joystick(0);
	Timer timer = new Timer();
	// final int frontLeftMotor, rearLeftMotor, frontRightMotor, rearRightMotor)
	RobotDrive myRobot = new RobotDrive(leftfront, leftback, rightfront, rightback);
	private Gyro gyro = new ADXRS450_Gyro();

	/**
	 * This function is run when the robot is first started up and should be used
	 * for any initialization code.
	 */
	@Override
	public void robotInit() {
		leftfront.setInverted(true);
		leftback.setInverted(true);
		rightfront.setInverted(true);
		rightback.setInverted(true);
		gyro.calibrate();
	}

	/**
	 * This function is run once each time the robot enters autonomous mode
	 */
	@Override
	public void autonomousInit() {
		timer.reset();
		// timer.start(); started in autonomous now
		myRobot.drive(0, 0);
		leftEncoder.setDistancePerPulse(1);
		rightEncoder.setDistancePerPulse(1);
		leftEncoder.reset();
		rightEncoder.reset();
		autocomplete = false;
		gyro.calibrate();

	}

	public double recalcDistance() {
		double distanceIn;
		int count = 0;
		distanceIn = leftEncoder.getDistance() / 6.325;
		if (count++ == 75) {
			System.out.println(distanceIn);
			count = 0;

		}
		return distanceIn;

	}

	// ----------------------------------------------------------------------------------------------
	double Feather(double totalDist, double distTraveled) {
		double returnValue = 0;

		if (totalDist == 0)
			return 0;
		returnValue = (totalDist - distTraveled) / totalDist;

		if (returnValue >= 1)
			return 1;
		else if (returnValue <= -1)
			return -1;
		else if (returnValue < .5 && returnValue > .01)
			return 0.5;
		else if (returnValue < -.5 && returnValue > -.01)
			return -0.5;
		else
			return returnValue;
		// ------------------------------------------------------------------------------------------

	}

	public double Straighten() {
		double a, b, value;
		a = leftEncoder.getDistance();
		b = rightEncoder.getDistance();
		value = (a + b) / -200; // competition bot 2017
		// value = (a+b)/200; //practice bot
		return value;
	}

	// -----------------------------------------------------------------------------------------------
	public boolean timeLeft() {
		if (timer.get() < 100) // 18.6 on last years robot
			return true;
		else {
			return false;
		}
	}

	public void drive(double totalDist, double speed) {
		speedFactor = 1;
		double featherValue;
		do {
			featherValue = Feather(totalDist, recalcDistance());
			double s = speed * speedFactor * featherValue;
			if (s >= 0) {
				myRobot.drive(s, 1 * Straighten());
			} else {
				myRobot.drive(s, -1 * Straighten());
			}
		} while ((featherValue > 0.02) && timeLeft());
		stop();
	}

	// -----------------------------------------------------------------------------------------------
	public void turn(double angle, double speed) {
		gyro.reset();
		leftEncoder.reset();
		rightEncoder.reset();
		turnSpeedFactor = .50;
		double targetHeading = gyro.getAngle() + angle;

		double remainingTurn = targetHeading - gyro.getAngle();

		myRobot.setSafetyEnabled(true); //

		// System.out.println("Remaining turn " + remainingTurn + " Speed " + speed + "
		// Turn Speed " + turnSpeedFactor);
		while (Math.abs(remainingTurn) > 0.05 && timeLeft()) {
			pause(0.03);
			remainingTurn = targetHeading - gyro.getAngle();
			if (Math.abs(remainingTurn) <= 1) {
				turnSpeedFactor = 0.3;
			} else if (Math.abs(remainingTurn) <= 2.5) {
				turnSpeedFactor = 0.32;
			} else if (Math.abs(remainingTurn) <= 5) {
				turnSpeedFactor = 0.34;
			} else if (Math.abs(remainingTurn) <= 10) {
				turnSpeedFactor = 0.36;
			} else if (Math.abs(remainingTurn) <= 20) {
				turnSpeedFactor = 0.38;
			} else if (Math.abs(remainingTurn) <= 30) {
				turnSpeedFactor = 0.40;
			} else if (Math.abs(remainingTurn) <= 40) {
				turnSpeedFactor = 0.50;
			} else if (Math.abs(remainingTurn) <= 60) {
				turnSpeedFactor = 0.50;
			}
			myRobot.arcadeDrive(0.0,
					(remainingTurn < 0.0) ? turnSpeedFactor * -1 * speed : 1 * speed * turnSpeedFactor);
		}
		// System.out.println("Remaining turn " + remainingTurn + " Speed " + speed + "
		// Turn Speed " + turnSpeedFactor);
		System.out.println("right " + rightEncoder.getDistance() + " left " + leftEncoder.getDistance());
		stop();
	}

	// -----------------------------------------------------------------------------------------------
	public void stop() {
		myRobot.drive(0, 0);
		leftEncoder.reset();
		rightEncoder.reset();
		gyro.reset();
		pause(1);
	}

	// -----------------------------------------------------------------------------------------------
	public void pause(double waitTime) {
		// try {
		// Thread.sleep(waitTime);
		// } catch (InterruptedException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		Timer.delay(waitTime);
	}

	/**
	 * This function is called periodically during autonomous
	 */
	@Override
	public void autonomousPeriodic() {
		timer.start();
		if (!autocomplete) {
			/**
			 * drive(80, 0.3); turn(180,1); drive(80, 0.3); turn(-180,1); drive(245,.35);
			 * turn(-90,1); drive(160, 0.25); turn(-90, 1); drive(245, 0.35); turn(-90,1);
			 * drive(160, 0.25); turn(-90,1);
			 **/
			turn(180, 1);
			turn(180, 1);
			turn(180, 1);
			turn(180, 1);
			turn(-180, 1);
			turn(-180, 1);
			turn(-180, 1);
			turn(-180, 1);

			autocomplete = true;
		}
		// System.out.println("gyro " + gyro.getAngle());

	}

	/**
	 * This function is called once each time the robot enters tele-operated mode
	 */
	@Override
	public void teleopInit() {

	}

	/**
	 * This function is called during disable
	 */
	@Override
	public void disabledInit() {
		stop();
	}

	@Override
	public void disabledPeriodic() {
		stop();
	}

	/**
	 * This function is called periodically during operator control
	 */
	@Override
	public void teleopPeriodic() {
		myRobot.arcadeDrive(stick);
		// leftEncoder.getRate();
		rightEncoder.getRate();
		// System.out.println("Left" + leftEncoder.getRate());
		// System.out.println("Right"+ rightEncoder.getRate());
	}

	/**
	 * This function is called periodically during test mode
	 */
	@Override
	public void testPeriodic() {
		LiveWindow.run();
	}
}
