/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package org.usfirst.frc.team5826.robot.commands;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;

import org.usfirst.frc.team5826.robot.Robot;
import org.usfirst.frc.team5826.robot.subsystems.ExampleSubsystem;

/**
 * An example command.  You can replace me with your own command.
 */
public class ExampleCommand extends Command {
	private ExampleSubsystem subsystem;
	private DifferentialDrive drive;
	private Timer timer;
	
	public ExampleCommand() {
		requires(Robot.kExampleSubsystem);
	}

	@Override
	protected void initialize() {
		subsystem = Robot.kExampleSubsystem;
		drive = new DifferentialDrive(subsystem.rightDrive, subsystem.leftDrive);
		timer = new Timer();
		timer.start();
	}

	@Override
	protected void execute() {
		drive.arcadeDrive(0.2, 0);
	}

	@Override
	protected boolean isFinished() {
		return timer.get() > 5;
	}

	@Override
	protected void end() {
		drive.arcadeDrive(0.0, 0);
	}


	@Override
	protected void interrupted() {
		
	}
}
