package com.frc.team5826.y2018;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;

public class DifferentialDriveExample {
	
	
	
	public DifferentialDriveExample() {
		WPI_TalonSRX frontRight = new WPI_TalonSRX(1);
		WPI_TalonSRX backRight = new WPI_TalonSRX(2);
		WPI_TalonSRX frontLeft = new WPI_TalonSRX(3);
		WPI_TalonSRX backLeft = new WPI_TalonSRX(4);
				
		SpeedControllerGroup rightGroup = new SpeedControllerGroup(frontRight, backRight);
		SpeedControllerGroup leftGroup = new SpeedControllerGroup(frontLeft, backLeft);

		DifferentialDrive drive = new DifferentialDrive(leftGroup, rightGroup);
		
		drive.curvatureDrive(0, .5, true);
	}

}
