package com.frc.team5826.y2018;

import edu.wpi.first.wpilibj.DriverStation;

public class PowerUpPlateConfiguration {

	//Details pulled from...
	//https://wpilib.screenstepslive.com/s/currentCS/m/getting_started/l/826278-2018-game-data-details
	public void plateDetails() {
		//This will be something like LRL, RLR, RRR, etc
		//This is from the POV of your driver station
		String info = DriverStation.getInstance().getGameSpecificMessage();
		
		if(info == null || info.equals("") || info.length() != 3) {
			//What do you do if this is empty or not the write length
		}
		
		//To avoid case issues (R != r), switch everything to lowercase
		info = info.toLowerCase();
		
		for(int i = 0; i < info.length(); i++) {
			if(info.charAt(i) == 'l') {
				System.out.println("Plate " + i + " on left");
			}
			else if(info.charAt(i) == 'r'){
				System.out.println("Plate " + i + " on right");
			}
			else {
				System.out.println("Unknow character \"" + info.charAt(i) + "\" at pos " + i);
			}
		}
	}
	
}
